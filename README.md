# LowerIcons
Light-weight iOS tweak to lower the icons down the screen on the homescreen.

## Preview
<img src="https://gitlab.com/CardboardFace/lowericons/-/raw/master/Art/Settings%20preview.PNG" width="220">
<img src="https://gitlab.com/CardboardFace/lowericons/-/raw/master/Art/Homescreen%20preview.PNG" width="220">