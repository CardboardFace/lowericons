// Made by CardboardFace
// Credit to Cuboid: https://github.com/ryannair05/Cuboid-1
// And KritanaDev (HomeBeta): https://gist.github.com/KritantaDev/c1f059278aa4444d94d2e3783e481ed4

#define kPreferencePath @"/var/mobile/Library/Preferences/com.cardboardface.lowericons.prefs.plist" // Path for tweak preferences file

/* Dependencies for respring button */
@interface NSTask : NSObject
    @property (copy) NSString *launchPath;

    - (id)init;
    - (void)launch;
@end


// Preferences
static NSMutableDictionary *settings;
static bool tweakEnabled;
static bool hideIconLabels;
static CGFloat topOffset; // Icon list margin from screen top
static CGFloat verticalPadding; // Vertical padding between icons

%hook SBRootIconListView
	-(CGFloat)verticalIconPadding {
		return verticalPadding;
	}
%end

%hook SBIconView 
	-(void)setLabelHidden:(BOOL)arg1 {
		return %orig(hideIconLabels);
	}
%end

%hook SBIconListGridLayoutConfiguration 

	-(UIEdgeInsets)portraitLayoutInsets {
		UIEdgeInsets x = %orig;
		
		// Prevent tweak effecting folders
		NSUInteger rows = MSHookIvar<NSUInteger>(self, "_numberOfPortraitRows");
		NSUInteger columns = MSHookIvar<NSUInteger>(self, "_numberOfPortraitColumns");
		if (!(rows > 3 && columns == 4))
		{
			return x;
		}

		return UIEdgeInsetsMake(
			x.top + topOffset,
			x.left,
			x.bottom - topOffset + verticalPadding*2, // * 2 because regularly it was too slow
			x.right
		);
	}
%end


static void refreshPrefs() {

	// Ensure preferences file has been created (if not load, load default preferences from Prefs)
	CFArrayRef keyList = CFPreferencesCopyKeyList(CFSTR("com.cardboardface.lowericons.prefs"), kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
	if (keyList) {
		settings = (NSMutableDictionary *)CFBridgingRelease(CFPreferencesCopyMultiple(keyList, CFSTR("com.cardboardface.lowericons.prefs"), kCFPreferencesCurrentUser, kCFPreferencesAnyHost));
		CFRelease(keyList);
	} else {
		settings = nil;
	}
	if (!settings) {
		settings = [NSMutableDictionary dictionaryWithContentsOfFile:kPreferencePath];
	}

	// Load settings into vars (loading a default value if no setting is set)
	tweakEnabled = 							[([settings objectForKey:@"Enabled"] ?: @(YES)) boolValue];
	hideIconLabels = 						[([settings objectForKey:@"HideIconLabels"] ?: @(NO)) boolValue];
	topOffset = 							[([settings objectForKey:@"TopOffset"] ?: [NSNumber numberWithInt:240]) integerValue];
	verticalPadding = 						[([settings objectForKey:@"VerticalPadding"] ?: [NSNumber numberWithInt:20]) integerValue];
}
static void PreferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) {
	const bool hideIconLabelsOrig = hideIconLabels;
	refreshPrefs(); // Refresh prefs
	
	// Check if hideIconLabels has changed
	if (hideIconLabels != hideIconLabelsOrig) {
		UIAlertController *alertController = [UIAlertController
				alertControllerWithTitle:@"Respring?"
				message:@"This setting requires a respring"
				preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction *okAction = [UIAlertAction
				actionWithTitle:NSLocalizedString(@"Respring", @"OK action")
				style:UIAlertActionStyleDestructive
				handler:^(UIAlertAction *action)
					{
						// Respring:
						NSTask *task = [[NSTask alloc] init];
						[task setLaunchPath:@"/usr/bin/sbreload"];
						[task launch];
					}];
		UIAlertAction *laterAction = [UIAlertAction
				actionWithTitle:NSLocalizedString(@"Later", @"Later action")
				style:UIAlertActionStyleCancel
				handler:^(UIAlertAction *action) { } ];
		[alertController addAction:laterAction];
		[alertController addAction:okAction];
		[[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alertController animated:YES completion:nil];
	}
}



// Tweak constructor
%ctor {

	// Add observer to call PreferencesChangedCallback() when the preferences are updated
	CFNotificationCenterAddObserver(
			CFNotificationCenterGetDarwinNotifyCenter(),
			NULL,
			(CFNotificationCallback) PreferencesChangedCallback,
			CFSTR("com.cardboardface.lowericons.prefs.prefschanged"),
			NULL,
			CFNotificationSuspensionBehaviorCoalesce);

	refreshPrefs(); // Load preferences

	%init; // Initializes ungrouped hooks
}